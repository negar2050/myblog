<?php
return [
    "sms" => [
        "smsir" => [
            "base-url" => "https://api.sms.ir/v1",
            "api-key" => env("API_KEY_SMS_IR"),
            "default-sender" => env("default_sender"),
            "template-id" => "296190"
        ]
    ]
        ];
