<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/styles.css">

    <title>ورود/عضویت به حساب کاربری</title>
    
</head>
<body>
  @if($errors->all())
  <div class="error">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
  @endif

  @if(Session::has('status'))
  <div class="message">
   <p>{{ Session::get('status') }}</p>
  </div>
  @endif
  
  <div class="outerbox">
    <div class="box">
      <h2 class = "header" >ورود</h2>
          <form action="{{route('login-form')}}" method="post">
             @csrf
              <div class="input">
                <input value="{{old("mobileNumber")}}" type="tel" name="mobileNumber" placeholder="e.g : 09123456789" id="input">
                <label for="input">:شماره موبایل</label>
              </div>
              <div class="input">
                <input type="password" name="password" id="input">
                <label for="input">:پسوورد</label>
              </div>
              <div class="button">
                <button type="submit">ورود</button>
              </div>
          </form>
    </div>

    <div class="box">
      <h2 class = "header" >عضویت </h2>
          <form action="{{route('register-form')}}" method="post">
             @csrf
              <div class="input">
                <input value="{{old("MobileNumber")}}" type="tel" name="MobileNumber" placeholder="e.g : 09123456789" id="input">
                <label for="input">:شماره موبایل</label>
              </div>
              <div class="button">
                <button type="submit">ادامه</button>
              </div>
          </form>
  </div>
</div>
</body>
</html>