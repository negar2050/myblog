<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/styles.css">

    <title>ورود/عضویت به حساب کاربری</title>
    
</head>
<body>
  @if($errors->all())
  <div class="error">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
  @endif
    <div class="box3">
        <h2 class = "header" >:ارسال کد تائید </h2>
            <form action="{{route('verify-form')}}" method="post">
               @csrf
                <div class="input">
                  <input  type="tel" name="code" id="input">
                  <label for="input">:کد تائید</label>
                </div>
                <div class="button">
                  <button type="submit">ادامه</button>
                </div>
            </form>
    </div>
</body>
</html>