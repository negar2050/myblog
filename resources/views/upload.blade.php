<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/imagestyles.css">

    <title>صفحه اصلی</title>

</head>
<body>
  @php
    $mobileNumber = auth()->user()->mobile_number;
  @endphp
  <nav>
    <strong>{{$mobileNumber}}</strong>
    <a href="/logout">خروج</a>
  </nav>


  @if($errors->all())
  <div class="error">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
  @endif


  @if(Session::has('status'))
  <div class="message">
   <p>{{ Session::get('status') }}</p>
  </div>
  @endif



    <div class="boxupload">
        <h1 class = "header" > بارگذاری عکس:</h1>
            <form action="{{route('upload-form')}}" method="post" enctype="multipart/form-data">
               @csrf
               <div class="image">
                 <input type="file" name="image">
               </div>
               <div class="button">
                <button type="submit">ارسال</button>
              </div>
            </form>
    </div>

    <div class="outerboximages">
                @if ($imageIds != null)
                <form  target=_blank action="{{route('image-form')}}" method="post">
                @csrf
                @foreach ($imageIds as $imageId)
                <div class="boximage">
                  <label>
                    <input type="radio" name="selectedImage" value="{{$imageId}}" >
                    {{-- <img src="/{{$path}}" alt="{{$path}}"> --}}
                    <img src= "{{ route("image", $imageId) }}"  alt="{{$imageId}}">
                  </label>
                </div>
                @endforeach
    </div>


    <div class="boxconvert">
                <div class="radio">
                  <label for="radio1">عکس اصلی</label>
                  <input type="radio" name="radio" value="original" id="radio1"
                  @if(old("radio") == "original" ) checked  @endif>

                  <label for="radio2">سیاه و سفید</label>
                  <input type="radio" name="radio" value="B&W" id="radio2"
                  @if(old("radio") == "B&W" ) checked  @endif>

                  <label for="radio3">شطرنجی</label>
                  <input type="radio" name="radio" value="check" id="radio3"
                  @if(old("radio") == "check") checked  @endif>

                  <label for="radio4">هر دو</label>
                  <input type="radio" name="radio" value="both" id="radio4"
                  @if(old("radio") == "both") checked  @endif>
                </div>
                <div class="button">
                  <button type="submit">تبدیل</button>
                </div>

    </div>
                </form>
                @endif
</body>
</html>
