<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/styles.css">

    <title>ورود/عضویت به حساب کاربری</title>
    
</head>
<body>
  @if($errors->all())
  <div class="error">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
  @endif
    <div class="box3">
        <h2 class = "header" >:ایجاد پسوورد</h2>
            <form action="{{route('password-form')}}" method="post">
               @csrf
                <div class="input">
                  <input  type="password" name="password" id="input">
                  <label for="input">:پسوورد </label>
                </div>
                <div class="input">
                    <input  type="password" name="password_confirmation" id="input">
                    <label for="input">:تکرار پسوورد </label>
                  </div>
                <div class="button">
                  <button type="submit">ادامه</button>
                </div>
            </form>
    </div>
</body>
</html>