<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    
    protected $fillable = ["mobile_number", "password"];

    public function scopeCheckMobileExisting($query, $mobileNumber) {
        $query->where("mobile_number", $mobileNumber);
    }

    public function images() {
        return $this->hasMany(Image::class);
    }
}
