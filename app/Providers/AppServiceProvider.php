<?php

namespace App\Providers;

use App\Services\SmsProvider\SmsIr;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\Services\SmsProvider\SmsProvider;
// use resources\views\components\button;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(SmsProvider::class, SmsIr::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Blade::component('btn', button);

        Blade::directive('bold', function ($text) {
            return  "<b>" . $text ."</b>";
        });
        
        Blade::directive('underline', function ($text) {
            return  "<u>" . $text ."</u>";
        });
        Blade::if('is_int', function($value = null){
            return is_int($value);
        });
    }
}
