<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Services\ImageProcessor\ImageProcess;


class ImageController extends Controller
{
    public function home(Request $request) {
        $imageIds = Auth::user()->images->pluck("id")->toArray();
        session()->regenerate();
        return view("upload", compact('imageIds'));
    }

    public function showImages($imageId) {
        $imageIds = Auth::user()->images->pluck("id")->toArray();
        if(in_array($imageId, $imageIds)) {
            $path = Image::where("id", $imageId)->get()->implode("path");
            $contents = Storage::get($path);

            return Response()->make($contents, 200, [
                "Content-type" =>  "image"
            ]);
        }
            return abort(404);
    }

    public function imageUpload(Request $request) {
        $request->validate([
            "image" => "required|image|mimes:png,jpg",
        ]);
            $file = $request->file('image');
            // $extension = $file->getClientOriginalExtension();
            $extension = $file->extension();
            $path = Storage::putFileAs('uploads', $file, time() . "." . $extension);
            // $path = $file->storeAs('images/', time() . "." . $extension );
            // $contents = Storage::get($path);
            $url = Storage::url($path);

            $userId = auth()->user()->id;
            Image::create([
                "path" => $path,
                "user_id" => $userId
            ]);

            return redirect()->route("home");
    }

    public function image(Request $request, ImageProcess $imageProcess) {
        $request->validate([
            "radio" => "required|string|in:B&W,check,both,original",
            "selectedImage" => ["required",
            function($attribute, $value, $fail) {
                $imageIds = Auth::user()->images->pluck("id")->toArray();
                if(! in_array($value, $imageIds)) {
                    $fail("the " . $attribute . " does not belong to you.");
            }
        }]
        ]);
            $imageId = $request->get('selectedImage');
            $path = Image::where("id", $imageId)->get()->implode("path");
            $url = Storage::temporaryUrl($path, now()->addMinutes(1));
            // $url = storage_path()."/app/".$path;
            $imageType = getimagesize($url);

            if ($imageType["mime"] == "image/jpeg") {
                $image = imagecreatefromjpeg($url);
            } elseif ($imageType["mime"] == "image/png") {
                $image = imagecreatefrompng($url);
            }

            $convertType = $request->get('radio');
            if ($convertType == "B&W") {
                $imageProcess ->imageToBW($image);
            } elseif ($convertType == "check") {
                $imageProcess ->imageToCheck($image);
            } elseif ($convertType == "both") {
                $imageProcess ->imageToBW($image);
                $imageProcess ->imageToCheck($image);
            }

            ob_start();
            imagejpeg($image);
            $imageData = ob_get_contents();
            imagedestroy($image);
            ob_end_clean();

            return Response()->make($imageData, 200, [
                "Content-type" =>  "image"
            ]);
            // return response()->streamDownload(function() use ($imageData) {
            //     echo $imageData;
            // }, "result.jpg", [
            //     "Content-type" =>  "image"
            // ]);
        }
}

