<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Jobs\SendRegistrationSmsCode;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use App\Notifications\BeginningOfTheWeek;
use App\Services\SmsProvider\SmsIrVerify;
use Illuminate\Support\Facades\Notification;

class LoginController extends Controller
{
    public function showLoginForm() {
        if(auth()->check()) {
            session()->flash('status', 'You are logged in already!');
        }

        return view('login');
    }

    public function showVerifyForm() {

        return view('verify');
    }

    public function showPasswordForm() {

        return view('password');
    }


    public function register(Request $request, SmsIrVerify $smsIrVerify) {
        $request->validate([
            "MobileNumber" => ["required", "string", "regex:/^09\d{9}$/", "unique:users,mobile_number"
            //  function($attribute, $value, $fail) {
            //     $checkMobile = User::checkMobileExisting($value)->count();
            //     if($checkMobile != 0) {
            //         $fail("this " . $attribute . " is registered before.");
            //     }
            // }
            ]
        ]);
        $recepient = $request -> get("MobileNumber");
        $code = str_pad(strval(random_int(0,10000)), 4, "0", STR_PAD_RIGHT);
        $templateId = config("smsdata.sms.smsir.template-id");

        SendRegistrationSmsCode::dispatch($recepient, $templateId, $code)->onQueue("code_queue");

        $sessionId = Session::getId();
        Redis::setex("code.".$sessionId, 120, $code);
        session()->put("mobileNumber", $recepient);

        return redirect()->route("verify");

    }

    public function verify(Request $request) {
        $request->validate([
            "code" => ["required", "string", "digits:4",
            function($attribute, $value, $fail) {
                $sessionId = Session::getId();
                $checkCode = Redis::get("code.".$sessionId);
                if($checkCode != $value) {
                    $fail("this " . $attribute . " is not valid or has expired! try again!");
                }
            }
            ]
        ]);
        $sessionId = Session::getId();
        Redis::del("code.".$sessionId);
        Redis::setex("verifyStatus.".$sessionId, 5*60, "1");
        return redirect()->route("password");

    }

    public function password(Request $request) {
        $request->validate([
            'password' => 'required|confirmed|string|min:7|max:12'
        ]);

        $password = $request->password;
        $sessionId = Session::getId();
        $mobileNumber = session()->get("mobileNumber");

        $user = User::create([
            "mobile_number" => "$mobileNumber",
            "password" => Hash::make($password)
        ]);
        // $user = Auth::getProvider()->retrieveByCredentials($credentials);
        auth()->login($user);
        Redis::del("verifyStatus.".$sessionId);
        session()->flash('status', 'You are registered succesfully!');
        return redirect()->route("home");
    }
// ****************first login function*****************

    // public function login(Request $request) {
    //
    //     $request->validate([
    //         "mobilenumber" => ["required", "string" , "regex:/^09\d{9}$/" ,
    //          function($attribute, $value, $fail) {
    //             $checkmobile = User::where("mobilenumber", $value)->count();
    //             if($checkmobile == 0) {
    //                 $fail("this " . $attribute . " is not registered yet.");
    //             } else {
    //                 session()->put("mobilenumber", $value);
    //             }
    //         }
    //     ],
    //         "password" => ["required", "string", "min:7", "max:12",
    //          function($attribute, $value, $fail) {
    //             $mobilenumber = session()->get("mobilenumber");
    //             $hashedpassword = User::where("mobilenumber", $mobilenumber)->first()->password;
    //             $checkpassword = Hash::check($value, $hashedpassword);
    //             if(!$checkpassword) {
    //                 $fail("this " . $attribute . " is not valid.");
    //             }
    //         }
    //         ]
    //     ]);

    //     session()->flash('status', 'You are logged in succesfully!');
    //     return redirect()->route("home");

    //     }

// ****************second login function******************

    public function login(Request $request) {
        $request->validate([
            "mobileNumber" => ["required", "string" , "regex:/^09\d{9}$/" ,
                        function($attribute, $value, $fail) {
                        $checkMobile = User::checkMobileExisting($value)->count();
                        if($checkMobile == 0) {
                            $fail("this " . $attribute . " is not registered yet.");
                        }

                    }],
            "password" => ["required", "string", "min:7", "max:12"]
        ]);

        $mobileNumber = $request->get("mobileNumber");
        $password = $request->get("password");
        $user = [
            "mobile_number" => $mobileNumber,
            "password" => $password
        ];

        if (Auth::attempt($user)) {
            session()->flash('status', 'You are logged in succesfully!');
            Auth::logoutOtherDevices($password);
            return redirect()->route("home");
        }

        return back()->withErrors([
            'mobileNumber' => 'The provided credentials do not match our records!',
        ])->onlyInput('mobileNumber');
    }

    public function logout() {
        Auth::logout();
        session()->invalidate();
        session()->regenerateToken();
        session()->flash('status', 'You are logged out succesfully!');
        return redirect()->route("login");
    }


}

