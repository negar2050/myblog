<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class Verification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $sessionId = Session::getId();
        if(Redis::exists("code.".$sessionId) == 0) {
            if(auth()->check()) {
                session()->flash('status', 'You are logged in already!');
                return redirect()->route('login')  ;
            }
            return redirect()->route('login')->withErrors([
                'status' => 'Your code has expired! try again!',
            ])->onlyInput('status');
        }

        return $next($request);
    }
}
