<?php
namespace App\Services\SmsProvider;
use Illuminate\Support\Facades\Http;
use Illuminate\Notifications\Notification;


class SmsIrGroup {
    public function send($notifiable, Notification $notification) {
        $response = Http::withheaders([
                    "ACCEPT" => "application/json",
                    "X-API-KEY" => config("smsdata.sms.smsir.api-key")
                ]) -> acceptJson() ->post(config("smsdata.sms.smsir.base-url") . "/send/bulk", [
                    "lineNumber" => intval(config("smsdata.sms.smsir.default-sender")),
                    "MessageText" => $notification->toSms($notifiable),
                    "Mobiles" => [$notifiable->mobile_number]
                ]);
                return $response->json();
            }
}
