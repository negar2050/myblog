<?php

namespace App\Services\SmsProvider;


interface SmsProvider {
    public function sendSms(string $sender, string $recepient, string $text);
    public function sendDefault(string $recepient, string $text );
}