<?php
namespace App\Services\SmsProvider;
use Illuminate\Support\Facades\Http;


class SmsIr implements SmsProvider {
    public function sendSms(string $sender, string $recepient, string $text) {
        $response = Http::withheaders([
                    "ACCEPT" => "application/json",
                    "X-API-KEY" => config("smsdata.sms.smsir.api-key")
                ]) -> acceptJson() ->post(config("smsdata.sms.smsir.base-url") . "/send/bulk", [
                    "lineNumber" => intval($sender),
                    "MessageText" => $text,
                    "Mobiles" => [$recepient]
                ]);
                return $response->json();
            }
    public function sendDefault(string $recepient, string $text ) {
        return $this->sendSms(config("smsdata.sms.smsir.default-sender"), $recepient, $text);
    }
    
}
