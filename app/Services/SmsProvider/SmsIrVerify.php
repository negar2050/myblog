<?php
namespace App\Services\SmsProvider;
use Illuminate\Support\Facades\Http;


class SmsIrVerify {
    public function sendSms(string $recepient, int $templateId, string $code) {
        $response = Http::withheaders([
                    "ACCEPT" => "application/json",
                    "X-API-KEY" => config("smsdata.sms.smsir.api-key")
                ]) -> acceptJson() ->post(config("smsdata.sms.smsir.base-url") . "/send/verify", [
                    "mobile" => $recepient,
                    "templateId" => $templateId,
                    "parameters" => [
                        [
                            "name" => "CODE",
                            "value" => (string)$code
                             ]
                    ]
                ]);
                return $response->json();
            }   
}
