<?php
namespace App\Services\ImageProcessor;



class ImageProcess {
    public function imageToBW ($image) {
        $width = imagesx($image);
        $height = imagesy($image);
        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $rgb = imagecolorat($image, $x, $y);
                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8 ) & 0xFF;
                $b = $rgb & 0xFF;
                $gray = round(($r + $g + $b) / 3);
                $color = imagecolorallocate($image, $gray, $gray, $gray);
                imagesetpixel($image , $x, $y, $color);        
            }
        }
        return $image;
    }

    public function imageToCheck ($image, $x_rec = 30, $y_rec = 30) {
        $width = imagesx($image);
        $height = imagesy($image);
        for ($i = 0; $i < $width; $i += $x_rec ) {
            for ($j = 0; $j < $height; $j += $y_rec) {
                $z = 0;
                $r = [];
                $g = [];
                $b = [];
                for ($x = $i; $x < $i + $x_rec; $x++) {
                    if ($x >= $width) {
                        continue;
                    }
                    for ($y = $j; $y < $j + $y_rec; $y++) {
                        if ($y >= $height) {
                            continue;
                        }
                        $rgb = imagecolorat($image, $x, $y);
                        $r[$z] = ($rgb >> 16) & 0xFF;
                        $g[$z] = ($rgb >> 8 ) & 0xFF;
                        $b[$z] = $rgb & 0xFF;
                        $z++;      
                    }
                }
                $rr = round(array_sum($r)/count($r));
                $gg = round(array_sum($g)/count($g));
                $bb = round(array_sum($b)/count($b));
                $color = imagecolorallocate($image, $rr, $gg, $bb);
                    for ($x = $i; $x < $i + $x_rec; $x++) {
                        if ($x >= $width) {
                            continue;
                        }
                        for ($y = $j; $y < $j + $y_rec; $y++) {
                            if ($y >= $height) {
                                continue;
                            }
                            imagesetpixel($image , $x, $y, $color);  
                        }
                    }  
            }
        } 
        return $image;  
    }

}
