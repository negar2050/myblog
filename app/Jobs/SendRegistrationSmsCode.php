<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\SmsProvider\SmsIrVerify;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SendRegistrationSmsCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $recepient;
    public $templateId;
    public $code;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recepient, $templateId, $code)
    {
        $this->recepient = $recepient;
        $this->templateId = $templateId;
        $this->code = $code;
    }

    public $tries = 1;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SmsIrVerify $smsIrVerify)
    {
        $smsIrVerify->sendSms($this->recepient, $this->templateId, $this->code);
    }
}
