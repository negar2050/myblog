<?php

namespace App\Console;

use App\Models\User;
use App\Models\Image;
use App\Notifications\BeginningOfTheWeek;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected function scheduleTimezone() {
        return 'Asia/Tehran';
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        $schedule->call(function () {
            $text = "سلام, اول هفته خوبی داشته باشید.";
            $userIds =Image::minOneImageUserIds()->pluck("user_id")->toArray();
            $users = User::find($userIds);
            Notification::send($users, (new BeginningOfTheWeek($text))->onQueue("sms_queue"));
        })->weekly()->saturdays()->at('10:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
