<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\LoginController;
// use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// *********Authentication********
Route::get('/login', [LoginController::class, "showLoginForm"]) ->name("login");
Route::get('/logout', [LoginController::class, "logout"]) ->name("logout");

Route::post('/register/verify', [LoginController::class, "register"]) ->name('register-form');
Route::get('/register/verify', [LoginController::class, "showVerifyForm"]) ->name("verify") ->middleware("verification");

Route::post('/register/password', [LoginController::class, "verify"]) ->name('verify-form');
Route::get('/register/password', [LoginController::class, "showPasswordForm"]) ->name("password") ->middleware("password.set");;

Route::get('/home', [ImageController::class, "home"]) ->name("home") ->middleware(["auth", "auth.session"]);

Route::post('/image', [ImageController::class, "image"]) ->name('image-form');
Route::get('/image/{imageId}', [ImageController::class, "showImages"]) ->name('image') ->middleware("auth");

//routes redirect to route("home") directly //
Route::post('/login/home', [LoginController::class, "login"]) ->name('login-form');
Route::post('/register/home', [LoginController::class, "password"]) ->name('password-form');
Route::post('/image/upload', [ImageController::class, "imageUpload"]) ->name('upload-form');





